<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\AddBook;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Expr\Cast\Int_;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class BookController extends AbstractController
{
    /**
     * @Route("/books", name="book_list", methods={"GET"})
     */
    public function index(EntityManagerInterface $em): Response
    {
        // Récupérer la liste des livres enregistrés dans la base de données
        // $books = $this->getDoctrine()->getRepository(Book::class)->findAll();
        $repository = $em->getRepository('App\Entity\Book');

        $books = $repository->findAll();

        // Afficher la liste des livres sous forme de texte
        return $this->render('book/index.html.twig', array(
            'books' => $books
        ));
    }

    /**
     * @Route("/books/add", name="add_book", methods={"GET", "POST"})
     */
    // public function add(Request $request): Response
    public function add(Request $request, EntityManagerInterface $em): Response
    {
        //Créer un objet de type book
        $book = new Book();

        // Traiter la soumission du formulaire d'ajout de livre
        $form = $this->createForm(AddBook::class, $book, options: array(
            'action' => $this->generateUrl('add_book'),
            'method' => 'POST'
        ));

        $form->handleRequest($request);

        //Traiter le formulaire soumis et valide 
        if($form->isSubmitted() && $form->isValid()) {
            //Enregistrement en base
            $em->persist($book); // Dit à doctrine "la y a une entité qui a subit des modification" (il ne se passe rien en base)
            $em->flush(); // Ajoute les modifications en base
        }

        // Afficher le formulaire d'ajout de livre
        return $this->render('book/add_book.html.twig', array(
            'form' => $form,
        ));
    }

    /**
     * 
     */
    #[Route('/books/{id}', name: 'single_book', methods: ['GET', 'DELETE', 'PUT'], requirements: ['id' => '\d+'])]
    public function singleBook(EntityManagerInterface $em, int $id): Response
    {
        $repository = $em->getRepository('App\Entity\Book');

        $book = $repository->find($id);

        // Vérifier si le livre existe
        if (!$book) {
            throw $this->createNotFoundException('Le livre demandé n\'existe pas.');
        }

        // Afficher la liste des livres sous forme de texte
        return $this->render('book/single.html.twig', array(
            'book' => $book
        ));

    }

    #[Route('/books/{id}', name: 'delete_book', methods: ['DELETE', 'POST'], requirements: ['id' => '\d+'])]
    public function deleteBook(EntityManagerInterface $em, int $id): Response
    {
        $book = $em->getRepository(Book::class)->find($id);

        if (!$book) {
            throw $this->createNotFoundException('Livre introuvable');
        }

        $em->remove($book);
        $em->flush();

        $this->addFlash('success', 'Le livre a été supprimé avec succès');

        return $this->redirectToRoute('book_list');
    }
 
 }
 

 