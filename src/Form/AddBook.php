<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AddBook extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title', 
            TextType::class,
            array(
                'label' => 'Titre du livre'
            )
        )->add(
            'publishedDate', 
            DateType::class,
            array(
                'label' => 'Date de publication',
                'widget' => 'single_text'
            )
        )->add(
            'submit', 
            SubmitType::class,
            array(
                'label' => 'Envoyer'
            )
        );
    }

}


