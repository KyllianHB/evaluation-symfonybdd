<?php

namespace App\Entity;

// use App\Repository\BookRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert; // Permet de gérer les contraintes

#[ORM\Entity]
#[ORM\Table(name: 'books')]

class Book
{

    #[ORM\Column(name: 'id', type: 'integer')]
    #[ORM\Id] // Clé primaire
    #[ORM\GeneratedValue] // auto incrementation
    private ?int $id = null;

    #[ORM\Column(name: 'title', type: 'string', length: 300)]
    #[Assert\NotBlank()]
    private ?string $title = null;
    
    #[ORM\Column(name: 'publishedDate', type: 'date')]
    #[Assert\LessThanOrEqual("today")]
    private $publishedDate;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
       $this->title = $title;
       return $this;
    }

    public function getPublishedDate(): ?\DateTimeInterface
    {
        return $this->publishedDate;
    }

    public function setPublishedDate(\DateTimeInterface $publishedDate): self
    {
        $this->publishedDate = $publishedDate;

        return $this;
    }
}